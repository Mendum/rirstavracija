const { MongoClient } = require('mongodb');

//MongoDB connection

const url = 'mongodb+srv://bobi:123@cluster0.qqcyd.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
const dbName = 'vaje';

const conn_client = async (action_on_db) => {
    try {

        const client = new MongoClient(url);
        await client.connect();
        console.log('Connected successfully to server');

        db = client.db(dbName);
        console.log('Using db: ' + dbName);

        await action_on_db(db)

    } catch (error) {
        console.log(error);
    }
}

module.exports = conn_client;
