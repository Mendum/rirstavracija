const express = require('express');
const router = express.Router();
const mongo_client = require('../dbconn');
const { getAllRez, addNovoRez } = require('express/lib/application');
const ApiMetode = require('./metode');
let apiMetode = new ApiMetode();

//------------------------
//        rez
//------------------------

//GET
router.get('/', (req, res) => {
    try {

        mongo_client(async function (db) {
            const odg = await apiMetode.getAllRez();
        })

    } catch (error) {
        console.log(error);
        res.status(500).send('Napaka! pridobivanje podatkov\n');
    }
});

//POST
router.post('/add', (req, res) => {
    try {

        mongo_client(async function (db) {
            const odg = await apiMetode.addNovoRez(req.body.fname, req.body.lname, req.body.dDate);
            res.status(200).send(`Uspesno vstavljen dokument ${odg}\n`);
        })

    } catch (error) {
        console.log(error);
        res.status(500).send('Napaka! Nespesno vstavljen dokument\n');
    }
});

module.exports = router;