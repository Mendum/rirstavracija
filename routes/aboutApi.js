const express = require('express');
const router = express.Router();
const mongo_client = require('../dbconn');
const ApiMetode = require('./metode');
let apiMetode = new ApiMetode();
const { all } = require('express/lib/application');

//------------------------
//       About
//------------------------

//GET
router.get('/', (req, res) => {
    try {

        mongo_client(async function (db) {
            const odg = await apiMetode.getAbout();
            res.status(200).send(odg);
        })

    } catch (error) {
        console.log(error);
        res.status(500).send('Napaka! pridobivanje podatkov\n');
    }
});

//POST
router.post('/add', (req, res) => {
    try {

        mongo_client(async function (db) {
            const odg = await apiMetode.addAbout(req.body.zgodba);
            res.status(200).send(`Uspesno vstavljen dokument ${odg}\n`);
        })

    } catch (error) {
        console.log(error);
        res.status(500).send('Napaka! Nespesno vstavljen dokument\n');
    }
});

module.exports = router;