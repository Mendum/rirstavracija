const mongo_client = require('../dbconn');

class ApiMetode{
    async getAllMnenja(){
        const collection = db.collection('mnenja');
        const cursor = collection.find({});
        const cursor_values = await cursor.toArray();
        return cursor_values;
    }         

    async addNovoMnenje(fname, lname, message){
        const collection = db.collection('mnenja');
        const doc = [{ime: fname, priimek: lname, opis: message}];
        const result = await collection.insertMany(doc);
        return result.insertedId;
    }


    async getAbout(){
        const collection = db.collection('about');
        const cursor = collection.find({});
        const cursor_values = await cursor.toArray();
        return cursor_values;
    }         

    async addAbout(zgodba){
        const collection = db.collection('about');
        const doc = [{story: zgodba}];
        const result = await collection.insertMany(doc);
        return result.insertedId;
    }




    async getAllRez(){
        const collection = db.collection('rez');
        const cursor = collection.find({});
        const cursor_values = await cursor.toArray();
        res.send(cursor_values);
    }

    async addNovoRez(fname, lname, dDate){
        const collection = db.collection('rez');
        const doc = [{ime: fname, priimek: lname, dDate}];
        const result = await collection.insertMany(doc);
        return result.insertedId;
    }

    async sporocilo(){
        return "besedilo";
    }


    async getMeni(){
        const collection = db.collection('meni');
        const cursor = collection.find({});
        const cursor_values = await cursor.toArray();
        return cursor_values;
    }         

    async addMeni(naziv, cena, vrsta){
        const collection = db.collection('meni');
        const doc = [{name:naziv, price:cena, sort: vrsta }];
        const result = await collection.insertMany(doc);
        return result.insertedId;
    }
}      

module.exports = ApiMetode;