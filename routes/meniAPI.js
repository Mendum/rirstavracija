const express = require('express');
const router = express.Router();
const mongo_client = require('../dbconn');
const ApiMetode = require('./metode');
let apiMetode = new ApiMetode();
const { all } = require('express/lib/application');

//------------------------
//      Meni
//------------------------

//GET
router.get('/', (req, res) => {
    try {     

        mongo_client(async function (db) {
            const odg = await apiMetode.getMeni();
            res.status(200).send(odg);
        })

    } catch (error) {
        console.log(error);
        res.status(500).send('Napaka! pridobivanje podatkov\n');
    }
});

//POST
router.post('/add', (req, res) => {
    try {

        mongo_client(async function (db) {
            const odg = await apiMetode.addMeni(req.body.naziv,req.body.cena,req.body.vrsta );
            res.status(200).send(`Uspesno vstavljen dokument ${odg}\n`);
        })

    } catch (error) {
        console.log(error);
        res.status(500).send('Napaka! Nespesno vstavljen dokument\n');
    }
});

module.exports = router;