const app = require('../app');
const { request } = require('supertest');
const sinon = require('sinon');
const referee = require("@sinonjs/referee");
const assert = referee.assert;
const { dataRezervacija } = require('./data');
const ApiMetode = require('../routes/metode');
let apiMetode = new ApiMetode();

describe('Rezervacija endpoint test', () => {
    
    let stubGetAllRez;
    
    beforeEach(() => {
        stubGetAllRez = sinon.stub(apiMetode, 'getAllRez');
    });

    afterEach(function () {
        stubGetAllRez.restore(); 
    });

    it('Stub the api method', () => {
        //Arrange
        stubGetAllRez.withArgs().returns(dataRezervacija);
        //Act
        let res = apiMetode.getAllRez();
        //Assert
        assert.equals(stubGetAllRez(), jsonValue);

    });
});

let jsonValue = [
    {
    "_id": "d6accebf-be5b-4759-951f-f3a2a5825447",
    "ime": "Janez",
    "priimek": "Novak",
    "datum": "2022-1-3"
    },
    {
    "_id": "9afc9b74-387a-4dca-b07d-ed13920f5423",
    "ime": "Marija",
    "priimek": "Rajh",
    "datum": "2022-1-4"
    }
]