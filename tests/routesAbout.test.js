const app = require('../app');
const { request } = require('supertest');
const sinon = require('sinon');
const referee = require("@sinonjs/referee");
const assert = referee.assert;
const { dataAbout } = require('./data');
const ApiMetode = require('../routes/metode');
let apiMetode = new ApiMetode();

describe('About endpoint test', () => {
    
    let stubGetAbout;
    
    beforeEach(() => {
        stubGetAbout = sinon.stub(apiMetode, 'getAbout');
    });

    afterEach(function () {
        stubGetAbout.restore(); 
    });

    it('Stub the api method', () => {
        //Arrange
        stubGetAbout.withArgs().returns(dataAbout);
        //Act
        let res = apiMetode.getAbout();
        //Assert
        assert.equals(stubGetAbout(), jsonValue);

        stubGetAbout.restore();
    });
});

let jsonValue = [
    {
        "_id" : "9dde8d11-ec36-4bc4-9952-80530a078c91",
        "zgodba" : " 'La Bellezza Spagnola' oziroma po slovensko 'Španska lepotica' je restavracija z več kot dvestno letno tradicijo. Vsak obisk naše restavracije je prvovrstna kulinarična izkušnja. V naši restavraciji se bo kot doma počutil čisto vsak ljubitelj mediteranske hrane, naša ponudba pa je tako široka, da se za vsakega nekaj najde. Večerja v Le Bellezza Spagnola vas zagotovo ne bo razočala - zato ne odlašajte in hitro rezervirajte svojo mizo!"
      }
]