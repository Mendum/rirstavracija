const app = require('../app');
const { request } = require('supertest');
const sinon = require('sinon');
const referee = require("@sinonjs/referee");
const assert = referee.assert;
const { dataMnenje } = require('./data');
const ApiMetode = require('../routes/metode');
let apiMetode = new ApiMetode();

describe('Mnenja endpoint test', () => {
    
    let stubGetAllMnenja;
    
    beforeEach(() => {
        stubGetAllMnenja = sinon.stub(apiMetode, 'getAllMnenja');
    });

    afterEach(function () {
        stubGetAllMnenja.restore(); 
    });

    it('Stub the api method', () => {
        //Arrange
        stubGetAllMnenja.withArgs().returns(dataMnenje);
        //Act
        let res = apiMetode.getAllMnenja();
        //Assert
        assert.equals(stubGetAllMnenja(), jsonValue);

        stubGetAllMnenja.restore();
    });
});

let jsonValue = [
    {
        "_id": "3905e98b-f288-4c21-9808-18e7103f01d3",
        "ime": "Janez",
        "priimek": "Novak",
        "opis": "Pri vas sem se imel zelo dobro"
    },
    {
        "_id": "6009408d-429f-413a-bad2-551fd5e0dd39",
        "ime": "Marija",
        "priimek": "Rajh",
        "opis": "Odlicno za taksno ceno"
    }
]