const app = require('../app');
const { request } = require('supertest');
const sinon = require('sinon');
const referee = require("@sinonjs/referee");
const assert = referee.assert;
const { dataMeni } = require('./data');
const ApiMetode = require('../routes/metode');
let apiMetode = new ApiMetode();

describe('Meni endpoint test', () => {
    
    let stubGetAllMeni;
    
    beforeEach(() => {
        stubGetAllMeni = sinon.stub(apiMetode, 'getMeni');
    });

    afterEach(function () {
        stubGetAllMeni.restore(); 
    });

    it('Stub the api method', () => {
        //Arrange
        stubGetAllMeni.withArgs().returns(dataMeni);
        //Act
        let res = apiMetode.getMeni();
        //Assert
        assert.equals(stubGetAllMeni(), jsonValue);
   
        stubGetAllMeni.restore();
    });
});

let jsonValue = [
    {
        "_id": "61b182fd-e8d3-42fc-b85d-86dea00fac7b",
        "naziv": "Gobova juha",
        "cena": "3.99",
        "vrsta": "juhe"
      },
      {
        "_id": "3838b1c7-6baa-40ca-951d-b45df885bc21",
        "naziv": "Patata con polo",
        "cena": "7.99",
        "vrsta": "glavne jedi"
      },
      {
        "_id": "5b7724ab-7f22-41c5-a667-1db0623210cc",
        "naziv": "domac strudel",
        "cena": "2.99",
        "vrsta": "sladice"
      }
]