const dataMnenje = [
  {
    "_id": "3905e98b-f288-4c21-9808-18e7103f01d3",
    "ime": "Janez",
    "priimek": "Novak",
    "opis": "Pri vas sem se imel zelo dobro"
  },
  {
    "_id": "6009408d-429f-413a-bad2-551fd5e0dd39",
    "ime": "Marija",
    "priimek": "Rajh",
    "opis": "Odlicno za taksno ceno"
  }
]
  
const dataRezervacija = [
  {
  "_id": "d6accebf-be5b-4759-951f-f3a2a5825447",
  "ime": "Janez",
  "priimek": "Novak",
  "datum": "2022-1-3"
  },
  {
  "_id": "9afc9b74-387a-4dca-b07d-ed13920f5423",
  "ime": "Marija",
  "priimek": "Rajh",
  "datum": "2022-1-4"
  }
]

const dataAlergen = [
  {
    "_id": "1108367c-507a-42a6-95bd-53f81667b774",
    "naziv": "orescki",
    "opis": "aaa"
  },
  {
    "_id": "46534050-f2d7-4110-9726-77ed77e76937",
    "naziv": "gluten",
    "opis": "bbb"
  }
]
  
const dataMeni = [
  {
    "_id": "61b182fd-e8d3-42fc-b85d-86dea00fac7b",
    "naziv": "Gobova juha",
    "cena": "3.99",
    "vrsta": "juhe"
  },
  {
    "_id": "3838b1c7-6baa-40ca-951d-b45df885bc21",
    "naziv": "Patata con polo",
    "cena": "7.99",
    "vrsta": "glavne jedi"
  },
  {
    "_id": "5b7724ab-7f22-41c5-a667-1db0623210cc",
    "naziv": "domac strudel",
    "cena": "2.99",
    "vrsta": "sladice"
  }
]

const dataAbout =[ {
  "_id" : "9dde8d11-ec36-4bc4-9952-80530a078c91",
  "zgodba" : " 'La Bellezza Spagnola' oziroma po slovensko 'Španska lepotica' je restavracija z več kot dvestno letno tradicijo. Vsak obisk naše restavracije je prvovrstna kulinarična izkušnja. V naši restavraciji se bo kot doma počutil čisto vsak ljubitelj mediteranske hrane, naša ponudba pa je tako široka, da se za vsakega nekaj najde. Večerja v Le Bellezza Spagnola vas zagotovo ne bo razočala - zato ne odlašajte in hitro rezervirajte svojo mizo!"
}]

module.exports = {dataMnenje, dataRezervacija, dataAlergen, dataMeni,dataAbout};