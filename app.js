const express = require('express');
const path = require('path');
const app = express();
var bodyParser = require('body-parser');  

app.use(bodyParser.urlencoded({ extended: true })); 

//Server
app.use(express.json());
app.use(express.static('public'));  

const mnenjaMethods = require('./routes/mnenjaApi');
app.use('/opinion', mnenjaMethods);

const rezervacijaMethods = require('./routes/rezervacijaApi');
app.use('/reservation', rezervacijaMethods);


const aboutMethods = require('./routes/aboutApi');
app.use('/about', aboutMethods);


app.get('/domov', function(req, res) {
    res.sendFile(path.join(__dirname, 'pages/index.html'));
});

app.get('/mnenja', function(req, res) {
    res.sendFile(path.join(__dirname, 'pages/mnenja.html'));
});

app.get('/rezervacija', function(req, res) {
    res.sendFile(path.join(__dirname, 'pages/rez.html'));
});


app.get('/onas', function(req, res) {
    res.sendFile(path.join(__dirname, 'pages/onas.html'));
});



const meniMethods = require('./routes/meniAPI');
app.use('/menu', meniMethods);

app.get('/meni', function(req, res) {
    res.sendFile(path.join(__dirname, 'pages/meni.html'));
});


app.listen(3000, ()  =>       
    console.log('server started at localhost:3000')
);
//app.listen(3000)

module.exports = app;